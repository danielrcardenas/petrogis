package co.edu.udistrital.petrogis.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="GIS_PARAMETER")
public class GisParameter implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private BigDecimal id;
	private String gisName;
	private String gisValue;

	public GisParameter() {

	}

	@Id
	@Column(name = "ID")
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	@Column(name = "GIS_NAME")
	public String getGisName() {
		return gisName;
	}

	public void setGisName(String gisName) {
		this.gisName = gisName;
	}

	@Column(name = "GIS_VALUE")
	public String getGisValue() {
		return gisValue;
	}

	public void setGisValue(String gisValue) {
		this.gisValue = gisValue;
	}
	
}
