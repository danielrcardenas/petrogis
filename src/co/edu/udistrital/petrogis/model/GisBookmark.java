package co.edu.udistrital.petrogis.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GIS_BOOKMARK")
public class GisBookmark {

	private BigDecimal id;
	private String gisName;
	private String coords;

	public GisBookmark() {

	}

	@Id
	@Column(name = "ID")
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	@Column(name = "GIS_NAME")
	public String getGisName() {
		return gisName;
	}

	public void setGisName(String gisName) {
		this.gisName = gisName;
	}

	@Column(name = "COORDS")
	public String getCoords() {
		return coords;
	}

	public void setCoords(String coords) {
		this.coords = coords;
	}

}
