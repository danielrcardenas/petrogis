package co.edu.udistrital.petrogis.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="GIS_SERVICE")
public class GisService implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private BigDecimal id;
	private String gisType;
	private String gisUrl;
	private String gisLabel;
	private String visibility;
	private String gisId;
	private String alpha;
	private String mapType;
	private String bingKey;
	private String culture;
	private String style;
	private Integer gisOrder;
	private String tocAdded;

	public GisService() {

	}

	@Id
	@Column(name = "ID")
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	@Column(name = "GIS_TYPE")
	public String getGisType() {
		return gisType;
	}

	public void setGisType(String gisType) {
		this.gisType = gisType;
	}

	@Column(name = "GIS_URL")
	public String getGisUrl() {
		return gisUrl;
	}

	public void setGisUrl(String gisUrl) {
		this.gisUrl = gisUrl;
	}

	@Column(name = "GIS_LABEL")
	public String getGisLabel() {
		return gisLabel;
	}

	public void setGisLabel(String gisLabel) {
		this.gisLabel = gisLabel;
	}

	@Column(name = "VISIBILITY")
	public String getVisibility() {
		return visibility;
	}

	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	@Column(name = "GIS_ID")
	public String getGisId() {
		return gisId;
	}

	public void setGisId(String gisId) {
		this.gisId = gisId;
	}

	@Column(name = "ALPHA")
	public String getAlpha() {
		return alpha;
	}

	public void setAlpha(String alpha) {
		this.alpha = alpha;
	}

	@Column(name = "MAP_TYPE")
	public String getMapType() {
		return mapType;
	}

	public void setMapType(String mapType) {
		this.mapType = mapType;
	}

	@Column(name = "BING_KEY")
	public String getBingKey() {
		return bingKey;
	}

	public void setBingKey(String bingKey) {
		this.bingKey = bingKey;
	}

	@Column(name = "CULTURE")
	public String getCulture() {
		return culture;
	}

	public void setCulture(String culture) {
		this.culture = culture;
	}

	@Column(name = "STYLE")
	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}
	
	@Column(name = "GIS_ORDER")
	public Integer getGisOrder() {
		return gisOrder;
	}

	public void setGisOrder(Integer gisOrder) {
		this.gisOrder = gisOrder;
	}
	@Column(name = "TOC_ADDED")
	public String getTocAdded() {
		return tocAdded;
	}

	public void setTocAdded(String tocAdded) {
		this.tocAdded = tocAdded;
	}


}
