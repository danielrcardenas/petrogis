package co.edu.udistrital.petrogis.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="GIS_WIDGET")
public class GisWidget implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private BigDecimal id;
	private String gisLabel;
	private String gisUrl;
	private String gisConfig;
	private String left;
	private String top;
	private String right;
	private String bottom;
	private String icon;
	private Boolean container;
	private String preload;
	private String gisType;
	private Integer gisOrder;

	public GisWidget() {

	}

	@Id
	@Column(name = "ID")
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	@Column(name = "GIS_URL")
	public String getGisUrl() {
		return gisUrl;
	}

	public void setGisUrl(String gisUrl) {
		this.gisUrl = gisUrl;
	}

	@Column(name = "GIS_LABEL")
	public String getGisLabel() {
		return gisLabel;
	}

	public void setGisLabel(String gisLabel) {
		this.gisLabel = gisLabel;
	}

	@Column(name = "GIS_CONFIG")
	public String getGisConfig() {
		return gisConfig;
	}

	public void setGisConfig(String gisConfig) {
		this.gisConfig = gisConfig;
	}

	@Column(name = "LEFT")
	public String getLeft() {
		return left;
	}

	public void setLeft(String left) {
		this.left = left;
	}

	@Column(name = "TOP")
	public String getTop() {
		return top;
	}

	public void setTop(String top) {
		this.top = top;
	}

	@Column(name = "RIGHT")
	public String getRight() {
		return right;
	}

	public void setRight(String right) {
		this.right = right;
	}

	@Column(name = "BOTTOM")
	public String getBottom() {
		return bottom;
	}

	public void setBottom(String bottom) {
		this.bottom = bottom;
	}

	@Column(name = "ICON")
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	@Column(name = "CONTAINER")
	public Boolean getContainer() {
		return container;
	}

	public void setContainer(Boolean container) {
		this.container = container;
	}

	@Column(name = "PRELOAD")
	public String getPreload() {
		return preload;
	}

	public void setPreload(String preload) {
		this.preload = preload;
	}

	@Column(name = "GIS_TYPE")
	public String getGisType() {
		return gisType;
	}

	public void setGisType(String gisType) {
		this.gisType = gisType;
	}

	@Column(name = "GIS_ORDER")
	public Integer getGisOrder() {
		return gisOrder;
	}

	public void setGisOrder(Integer gisOrder) {
		this.gisOrder = gisOrder;
	}
}
