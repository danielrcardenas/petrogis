package co.edu.udistrital.petrogis.controller.bo;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import co.edu.udistrital.petrogis.model.GisParameter;
import co.edu.udistrital.petrogis.model.GisQuery;
import co.edu.udistrital.petrogis.model.GisService;
import co.edu.udistrital.petrogis.model.GisWidget;
import co.edu.udistrital.petrogis.model.dao.CustomHibernateDao;

@Service
public class GisServiceBo {

	@Resource
	private CustomHibernateDao dao;

	public List<GisService> getListService() {
		return dao.find("from GisService o Order by o.gisOrder");
	}
	public List<GisService> getExcludeTOCService() {
		return dao.find("from GisService o where o.tocAdded = '0' Order by o.gisOrder");
	}
	
	public List<GisParameter> getListParameter() {
		return dao.find(GisParameter.class);
	}
	
	public List<GisWidget> getListWidget() {
		return dao.find("from GisWidget o Order by o.gisOrder");
	}
	
	public List<GisQuery> getListQuery() {
		return dao.find(GisQuery.class);
	}
	
	public List<GisQuery> getListQuery(String gisName) {
		return dao.find("from GisQuery o where o.gisName = '" + gisName + "'");
	}

}
