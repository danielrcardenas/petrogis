package co.edu.udistrital.petrogis.controller.bo;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import co.edu.udistrital.petrogis.model.GisBookmark;
import co.edu.udistrital.petrogis.model.dao.CustomHibernateDao;


/**
 * 
 * Clase creada para hibernate y mapeo de la tabla bookmarks
 * almacena puntos de importancia en el mapa
 * 
 * @author Daniel
 *
 */
@Service
public class GisBookmarkBo {

	@Resource
	private CustomHibernateDao dao;

	public List<GisBookmark> getList() {
		return dao.find(GisBookmark.class);
	}

	public GisBookmark insert(GisBookmark entity) {
		BigDecimal id = dao.getId("GisBookmark");
		entity.setId(id);
		dao.persist(entity);
		return entity;
	}

	public void remove(GisBookmark entity) {
		dao.delete(entity);
	}

}
