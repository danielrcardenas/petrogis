# README #


### What is this repository for? ###

* This is the source code for the PetroGIS project. PetroGIS is an environment and Petroleum GIS information administration platform based on ESRI API as a result from an engineering tesis in Colombia. 

* Abstract: The Environmental Licenses (EL) are a prerequisite in order to develop petroleum exploration and extraction projects in Colombia, the government not only wants to propel the mining industry in order to improve the country economy but also protect the environment aspects from its adverse effects that impact communities in the countryside, moreover socialize the project plans are difficult in remote places. We present in this paper a consistent software prototype architecture and describe its develop life cycle in order to establish a reliable, flexible and scalable platform to share crucial information among specialists, control entities and community to assist them on their decision making identifying classified restricted zones, natural resources coordinate transformation according with Colombian systems. This process reduce the response time and cost to get an EL examination, supervise the civil work flows and its environment impact going further in social aspects.

* Resumen: Previa a la ejecución de obras civiles requeridas en la exploración y explotación petrolera en Colombia, es necesario obtener una Licencia Ambiental (LA). Las actividades mineras son de especial importancia para el gobierno ya que al incrementarse el volumen de recursos, el país  obtiene mayores ingresos, sin embargo estos deben estar subordinados al cuidado ambiental y la mitigación de los efectos adversos que puedan generar sobre las comunidades. Este artículo presenta la arquitectura de un prototipo de software que permite compartir información relevante entre los especialistas, entidades de control y comunidades como apoyo a la toma decisiones en el desarrollo de las actividades de exploración y explotación petrolera, describiendo la problemática multidisciplinaria identificada, la metodología utilizada para desarrollar una herramienta basada en los Sistemas de Información Geográfica (SIG) en el marco de la ingeniería software describiendo los pasos seguidos hasta alcanzar una arquitectura basada en componentes, y los componentes logrados tales como: la identificación de zonas de mayor impacto ambiental, identificación de recursos ambientales y la transformación de coordenadas acorde con los sistemas utilizados en Colombia. La plataforma desarrollada bajo una arquitectura flexible y escalable: reduce los costos y tiempos requeridos por las entidades que realizan actividades de análisis relacionadas con la explotación y exploración petrolera.


* Version: 1.0

### How do I get set up? ###

1. In Adobe Flash Builder 4.6, go to "File" -> "Import Flash Builder project..."
2. Keeping "File" option selected, click "Browse..." button.
3. Select flexviewer-3.1-src.zip downloaded in step 1, e.g. "C:\Documents and Settings\jack\My Documents\flexviewer-3.1-src.zip".
"Extract new project to:" textbox will be automatically set to location where the project source will reside, e.g. "C:\Documents and Settings\jack\Adobe Flash Builder 4.6\FlexViewer.
4. Click "Finish" button. Project will be created and displayed in the Package Explorer window of Adobe Flash Builder, e.g. in this case FlexViewer.
5. If prompted to upgrade the project (because it was created with a previous version of Flash Builder), click "OK"
6. If prompted to choose Flex SDK version, select "Flex 4.6.0" or higher
If needed, download API Library from http://links.esri.com/flex-api/latest-download.
8. Go to "Project" -> "Properties" -> "Flex Build Path".
Click "Add SWC" and navigate to the agslib-3.[*]-[YYYY-MM-DD].swc file.
### Contribution guidelines ###

* Writing tests
* Code review


### acknowledgment ###

* ESRI Colombia
* Universidad Distrital FJC.