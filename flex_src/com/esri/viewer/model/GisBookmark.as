package com.esri.viewer.model
{
	[Bindable]
	[RemoteClass(alias="co.edu.udistrital.petrogis.model.GisBookmark")]
	public class GisBookmark
	{
		public var id:Number;
		public var gisName:String;
		public var coords:String;
		
		public function GisBookmark()
		{
		}
	}
}