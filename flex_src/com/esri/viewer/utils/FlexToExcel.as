package com.esri.viewer.utils
{
	import com.as3xls.xls.ExcelFile;
	import com.as3xls.xls.Sheet;
	
	import flash.errors.IllegalOperationError;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.utils.ByteArray;
	
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.controls.dataGridClasses.DataGridListData;
	
	public class FlexToExcel 
	{ 
		
		public function FlexToExcel() 
		{ 
			throw new IllegalOperationError ("Class ExcelExporterUtil is static. You can't instance this"); 
		}
		
		static public function exportDataGrid (dt:DataGrid, filename:String="excel.xls", listData:DataGridListData=null):void { 
			var head:Array = new Array(); 
			for each (var item:DataGridColumn in dt.columns) 
			{ 
				head.push(item.headerText); 
			} 
			var data:Array = new Array(); 
			for each (var obj:Object in dt.dataProvider) 
			{ 
				var arr:Array=new Array(); 
				for each (var hd:DataGridColumn in dt.columns) 
				{ 
					arr.push(hd.itemToLabel(obj)); 
				} 
				data.push(arr); 
			} 
			export(head,data,filename); 
		} 
		
		static private function export(head:Array, data:Array,filename:String):void { 
			// Create sheet 
			var cols:int = head.length; 
			var rows:int = data.length+1; 
			var sheet:Sheet = new Sheet(); 
			sheet.resize(rows,cols); 
			// Header 
			var row:int=0; 
			var col:int=0; 
			for each (var item:String in head) 
			{ 
				sheet.setCell(row,col,item); 
				col++; 
			} 
			// Data 
			row=1; 
			col=0; 
			for each (var dataRow:Array in data) 
			{ 
				for each (var dataCol:String in dataRow) 
				{ 
					sheet.setCell(row,col,dataCol); col++; 
				} 
				col=0; 
				row++; 
			} 
			// Add sheet 
			var xls:ExcelFile = new ExcelFile(); 
			xls.sheets.addItem(sheet); 
			var bytes:ByteArray = xls.saveToByteArray(); 
			// Generate file 
			var fr:FileReference = new FileReference();
					
			fr.save(bytes, filename); 
		} 
	} 
}