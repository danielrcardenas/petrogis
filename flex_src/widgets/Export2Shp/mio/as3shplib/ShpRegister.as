package widgets.Export2Shp.mio.as3shplib
{
	import com.esri.ags.layers.supportClasses.Field;
	import com.esri.ags.utils.WebMercatorUtil;
	
	import flash.utils.ByteArray;
	
	import mx.utils.ObjectUtil;
	
	public class ShpRegister
	{
		import com.esri.ags.Graphic;
		
		
		public var type:String = Field.TYPE_STRING;
		public var value:String;
		public var graphic:Graphic;
		
		
	
		public function ShpRegister(valueIn:String, graphicIn:Graphic)
		{
			value= valueIn;
			graphic=  clone(graphicIn) as Graphic ;
			if (isWebMercator(graphic.geometry.spatialReference.wkid))
			{
				graphic.geometry	= WebMercatorUtil.webMercatorToGeographic(graphic.geometry);
			}
			
			
			
		}
		private function clone(source:Object):* {
			var copier:ByteArray= new ByteArray();
			copier.writeObject(source);
			copier.position = 0;
			return(copier.readObject());
		}
		private function isWebMercator(wkid:Number):Boolean
		{
			return wkid == 102100 || wkid == 3857 || wkid == 102113;
		}
	}
}