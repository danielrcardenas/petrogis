package widgets.Export2Shp.mio.as3shplib
{
	public class PrjWriter
	{
		import flash.utils.ByteArray;
		
		private var _wkid:int;
		
		private var _bytes:ByteArray = new ByteArray();
		
		public function PrjWriter(wkid:int)
		{
			_wkid = wkid;
		}
		
		/**
		 * Creates the ESRI projection file from the specified WKID
		 */
		public function createPrjFile():void {
			var wktString:String = WktStrings.spatialReference[_wkid];
			if (wktString != null) {
				_bytes.writeMultiByte(wktString, "iso-8859-1");
			} 
		}
		
		/**
		 * Returns the bytearray containing data written to the prj file.
		 */
		public function getBytes():ByteArray {
			return _bytes;
		}
	}
}