////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Esri
//
// All rights reserved under the copyright laws of the United States.
// You may freely redistribute and use this software, with or
// without modification, provided you include the original copyright
// and use restrictions.  See use restrictions in the file:
// <install location>/License.txt
//
////////////////////////////////////////////////////////////////////////////////
package widgets.Coordinate
{
	
	
	import mx.formatters.NumberBase;
	import mx.formatters.NumberFormatter;
	import mx.validators.NumberValidator;

//--------------------------------------
//  Other metadata
//--------------------------------------
/**
 * Utility class to pretty print decimal degree numbers.
 * @private
 */
public final class DMSToDeg
{
    // Constants to define the format.
    public static const LAT:String = "lat";

    public static const LON:String = "lon";

    /**
     * Utility function to format a decimal degree number into a pretty string with degrees, minutes and seconds.
     * @param decDeg the decimal degree number.
     * @param decDir "lat" for a latitude number, "lon" for a longitude value.
     * @return A pretty print string with degrees, minutes and seconds.
     */
    public static function format(DMSToDeg:String, decDir:String):Number
    {
		var value:Array = DMSToDeg.split(" ");
		var degrees:Number = 0;
		var minutes:Number = 0;
		var seconds:Number = 0;
		var dir:String = null;
		if(value.length==4)
		{
			dir = value.pop();
			seconds = Number(value.pop());
			minutes = Number(value.pop());
			degrees = Number(value.pop());
			
		}
		else 
			return 0;
		
		var DD:Number = (seconds/3600) + (minutes/60) + degrees;
		if(dir == 'S' || dir == 'W')
			DD = DD*-1;	
        
        return DD;
    }
}

}
